// task 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let combiArr = [...clients1, ...clients2];
let mySet = new Set(combiArr);
combiArr = [...mySet];
console.log(combiArr);
