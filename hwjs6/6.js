// цикл forEach это разновидность цикла for,используется для перебора элементов массива или коллекции

let array = ['hello', 'world', 56, '23', 25, null];

function filterBy(array, type){
    
    let filterArr = array.filter((element) => typeof element !== type);
    
    return filterArr
}

console.log(filterBy(array, 'string'))