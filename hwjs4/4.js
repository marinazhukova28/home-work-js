// Метод обьекта это действия, которые можно выполнить с объектами.Метод это свойство, содержащее определение функции.Методы это функции, хранящиеся как свойства объекта.

function createNewUser(){
    
    let newUser = {

        firstName: prompt("Enter your first name"),
        lastName: prompt("Enter last name"),
    
        getLogin(){
            return this.firstName.slice(0,1).toLowerCase() + "." + this.lastName.toLowerCase()
        },    
    };
    return newUser
}
let user = createNewUser();

console.log(user.getLogin());