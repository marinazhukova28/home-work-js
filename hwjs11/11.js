// для работы с input не рекомендуется использовать события клавиатуры,по тому что на современных устройствах есть возможность ввода текстом или с помощью копировать/вставить щелчком мыши.События клавиатуры же должны использоваться, если мы хотим обрабатывать взаимодействие пользователя именно с клавиатурой (в том числе виртуальной). К примеру, если нам нужно реагировать на стрелочные клавиши Up и Down или горячие клавиши (включая комбинации клавиш).


let btn = document.querySelectorAll(".btn");

 document.body.addEventListener("keypress",function (event){

     btn.forEach((element) =>{
        if(element.classList.contains("btn.active")){
            element.classList.remove("btn.active");
        }
        if (element.innerText.toLowerCase()===event.key.toLowerCase()){
            element.classList.add("btn.active");
         
        }
    })

 });