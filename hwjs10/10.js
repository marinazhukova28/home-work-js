let form = document.querySelector('.password-form');


form.onclick = function(e) {

    if (e.target.classList.contains("fa-eye")){
        e.target.classList.replace("fa-eye","fa-eye-slash");
        e.target.parentNode.childNodes[1].type = "password";
    } else if (e.target.classList.contains("fa-eye-slash")){
        e.target.classList.replace("fa-eye-slash","fa-eye");
        e.target.parentNode.childNodes[1].type = "text";
    }
};

const confirmButton = document.querySelector(".btn");
let password = document.getElementById("password");
let passwordConfirm = document.getElementById("password-confirm");

function verification( ){
    let val1 = password.value;
    let val2 = passwordConfirm.value;
    let errorMes = document.querySelector('.error');

    if( val1===val2 ){
        alert("You are welcome");
    } else {
        errorMes.style.display = 'block';
    }
};

confirmButton.addEventListener('click', verification);