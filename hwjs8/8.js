// Oбработчик событий-это функция,котоая срабатывает как реакция на поисходящее.

let keyBox = document.getElementById("key");
let priceBox = document.getElementById("price-box");
let wrongPrice = document.getElementById("wrong-price");

function onfocus(e) {
    keyBox.value = "";
    wrongPrice.innerHTML = "";
    keyBox.style.outlineColor = "green";
    keyBox.style.color = "green";
}

function onblur(e) {
    if (keyBox.value >= 0 && keyBox.value !== "") {
        keyBox.style.borderColor = "green";
        priceBox.insertAdjacentHTML("beforeend",  `<div class="price-block"><span class="span-price">${keyBox.value}</span><span id="close">x</span></div>`);
    } else {
        keyBox.style.borderColor = "red";
        wrongPrice.insertAdjacentHTML("afterbegin", `<div>Please enter correct price</div>`);
    }
}

document.addEventListener('click', function (e) {
    if (e.target && e.target.id == 'close') {
        keyBox.value = "";
        e.target.parentElement.remove()
    }
});

keyBox.addEventListener("change", onchange);
keyBox.addEventListener("focusout", onblur);
keyBox.addEventListener("focus", onfocus);