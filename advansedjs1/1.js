// Прототипное наследование работает таким образом: создается родитель, с более обобщающими параметрами, затем создается потомок, у которого более узкая специфика,но в тоже время он перенимает параметры родителя и дополняет их своими. Для нас это полезно тем, что не нужно дублировать код. А так же помогает скрыть некоторые параметры от всеобщего доступа (как черный ящик в самолете), тем самым защить от постороннего вмешательства и сделать интерфейс более интуитивным для пользователя,(тк лишнее и непонятное для него будет скрыто).

class Employee{
    constructor(name,age,salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName(){
        return `${this.name}`;
    } 

    setName(newName){
        this.name = newName;
    }
    
    getAge(){
        return `${this.age}`;
    }

    setAge(newAge){
        this.age = newAge;
    }

    getSalary(){
        return `${this.salary}`;
    }

    setSalary(newSalary){
        this.salary = newSalary;
    }
}

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name, age, salary);
        this.lang = lang;
    }

    getTripleSalary(){        
        return this.getSalary() * 3;
    }
}

const programmer1 = new Programmer("Ilon", 50, 1603,"eng");
const programmer2 = new Programmer("Bill", 65, 1293, "eng");
const programmer3 = new Programmer("Mark", 37, 1229, "eng");

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

console.log(programmer1.getTripleSalary());
console.log(programmer2.getTripleSalary());
console.log(programmer3.getTripleSalary());